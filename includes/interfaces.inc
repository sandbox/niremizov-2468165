<?php

/**
 * FTP_users_manager module uses this interface functions to interact with plugin.
 *
 * @author Niremizov <niremizov@yandex.ru>
 */
interface FTPUsersManagerHandlerInterface {

  /**
   * Check whatever plugin is ready to create/delete/edit ftp users.
   * @return boolean
   */
  public function setUp();

  /**
   * This method should add to $form['settings']['content'] - it's own setting form.
   */
  public function settingsForm(&$form, &$form_state);

  /**
   * This method should save it's own setting that were added inside settingsForm().
   */
  public function settingsFormSubmit(&$form, &$form_state);

  /**
   * Alters user_profile and user_register forms. This method
   * should add it's own fields inside $form['ftp_users_manager']['settings']
   * @param string $username
   *
   * @see ftp_users_manager_form_user_profile_form_alter().
   */
  public function userForm(&$form, &$form_state);

  /**
   *  Validation callback for ftp_users_manager_form_user_profile_form_alter.
   *  @see ftp_users_manager_form_user_profile_form_validate().
   */
  public function userFormValidate($form, &$form_sate);

  /**
   * This submit callback is used inside ftp_users_manager_form_user_profile_form_submit().
   *
   * Errors may be set through drupal_set_message.
   *
   * @see ftp_users_manager_form_user_profile_form_submit().
   */
  public function userFormSubmit($form, &$form_state);

  /**
   * This method generates text message with FTP connection instructions based on self::userForm().
   *
   * @param $form
   * @param $form_state
   * @param $language
   * @return string Translated plain text.
   */
  public function userFormConnectionInstructions($form, $form_state, $language);

}

/**
 * @TODO Probably this should also be coverted to interface?
 *
 * Parent class for all ftp user manager plugins.
 * Use this class so other modules would know how to edit ftp users by using any plugin.
 *
 * @author Niremizov <niremizov@yandex.ru>
 */
abstract class FTPUserManagerHandler implements FTPUsersManagerHandlerInterface {

  /**
   * @param string $username
   * @param string $password
   * @param string $homeDir
   *
   * @return object that extends FTPUsersManagerHandlerReponse
   */
  abstract function createNewUser($username, $password, $homeDir);

  /**
   * @param string $username
   *
   * @return object that extends FTPUsersManagerHandlerReponse
   */
  abstract function deleteUser($username);

  /**
   * @param string $username
   * @param string $password
   *
   * @return object that extends FTPUsersManagerHandlerReponse
   */
  abstract function changePassword($username, $password);

  /**
   * Lists all FTP users.
   *
   * @return object that extends FTPUsersManagerHandlerReponse
   */
  abstract function listUsers();

}

/**
 * Interface for responses of FTPUserManagerHandler, so we could handle messages and errors
 * in simmular way.
 *
 * @author Niremizov <niremizov@yandex.ru>
 */
interface FTPUsersManagerHandlerReponse {
  /**
   * Checks whatever reponse is succesful.
   *
   * @return boolean
   */
  public function isSuccess();

  /**
   * Get reponse message.
   *
   * @return string
   */
  public function getAnswer();
}
