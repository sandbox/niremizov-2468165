<?php

class BegetFTPUsersManager extends FTPUserManagerHandler {

  protected $apiUrl = 'https://api.beget.ru/api';
  protected $login = '';
  protected $password = '';

  const MAXUSERNAMELENGTH = 16;

  /**
   * Check whatever account login and password are set.
   *
   * @TODO Probably, we could move part of this function's code (set of login and pass proerties)
   * to __construct() method? And here would be placed only checking code?
   *
   * @see FTPUsersManagerHandlerInterface::setUp()
   */
  public function setUp() {
    $auth = variable_get('ftp_users_manager_auth', array('login' => '', 'pass' => ''));
    if (empty($auth['login']) || empty($auth['pass'])) {
      return FALSE;
    }
    $this->login = $auth['login'];
    $this->password = $auth['pass'];
    return TRUE;
  }

  public function settingsForm(&$form, &$form_state) {
    $auth = variable_get('ftp_users_manager_auth', array('login' => '', 'pass' => ''));
    // Other settings.
    $settings = variable_get('ftp_users_manager_instructions', array('protocol' => '', 'host' => '', 'port' => ''));

    $form['settings']['content']['login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#description' => t('Login to authenticate FTP manager.'),
      '#default_value' => !empty($auth['login']) ? $auth['login'] : '',
    );

    $form['settings']['content']['pass'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#description' => t('Password to authenticate FTP manager.'),
    );

    // Settings for user instructions.
    $form['settings']['content']['instructions'] = array(
      '#type' => 'fieldset',
      '#title' => t('User Instructions'),
      '#description' => t('Following settings are used to inform user how to setup a FTP client correctly.'),
      '#weight' => 5,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['settings']['content']['instructions']['protocol'] = array(
      '#type' => 'select',
      '#title' => t('File protocol'),
      '#options' => array(
        'ftp' => 'FTP',
        'sftp' => 'SFTP',
      ),
      '#default_value' => $settings['protocol'],
    );
    $form['settings']['content']['instructions']['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Host name'),
      '#size' => 40,
      '#default_value' => $settings['host'],
      '#description' => t('Provide host name to inform user how setup a FTP client correctly.'),
    );
    $form['settings']['content']['instructions']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#size' => 2,
      '#default_value' => !empty($settings['port']) ? $settings['port'] : 22,
    );
  }

  public function settingsFormSubmit(&$form, &$form_state) {
    $settings = $form_state['values']['settings']['content'];
    $auth = variable_get('ftp_users_manager_auth', array('login' => '', 'pass' => ''));

    // If password was set - save it , else ignore.
    $pass = !empty($settings['pass']) ? ftp_users_manager_encrypt($settings['pass']) : $auth['pass'];

    variable_set('ftp_users_manager_auth', array('login' => $settings['login'], 'pass' => $pass));

    variable_set('ftp_users_manager_instructions', array(
      'protocol' => $settings['instructions']['protocol'],
      'host' => filter_xss_admin($settings['instructions']['host']),
      'port' => filter_xss_admin($settings['instructions']['port']),
    ));
  }

  /**
   * @see FTPUsersManagerHandlerInterface::userForm()
   */
  public function userForm(&$form, &$form_state) {
    $user = $form['#user'];
    $username = !empty($user->ftp_username) ? $user->ftp_username : '';

    // @TODO add Ssh togle checkbox.
    $form['ftp_users_manager']['settings']['content'] = array(
      'ftp_login' => array(
        '#type' => 'textfield',
        '#field_prefix' => $this->login . '_',
        '#title' => t('FTP Login'),
        '#maxlength' => $this->getMaxUsernameLengthWithoutSuffix(),
        '#size' => 17
      ),
      'ftp_pass' => array(
        '#type' => 'textfield',
        '#title' => t('FTP Password'),
        '#size' => 17
      ),
      'ftp_dir' => array(
        '#type' => 'textfield',
        '#title' => t('FTP Home Directory'),
        '#size' => 40
      )
    );

    if (!empty($username)) {
      $this->userInfoForm($form['ftp_users_manager']['settings']['content'], $username);
    }
  }

  /**
   * @see FTPUsersManagerHandlerInterface::userFormValidate()
   */
  public function userFormValidate($form, &$form_state) {
    // Nothing to do here.
    $pass = $form_state['values']['ftp_pass'];
    $login = $form_state['values']['ftp_login'];
    $user = $form['#user'];

    if ((empty($user->ftp_username) && $form_state['values']['ftp_users_manager_given']) ||
      (!empty($user->ftp_username) && $form_state['values']['ftp_users_manager_given'] && !empty($pass))) {
      // If this is new user or just editing, check field.
      if (!(preg_match('/[A-Za-z]/', $pass) && preg_match('/[0-9]/', $pass)) || strlen($pass) < 6) {
        form_set_error('ftp_pass',t('FTP Password must contains letters and digits, should be longer than 6 characters.'));
      }
      if (empty($login)) {
        form_set_error('ftp_login',t('FTP Login cannot be empty.'));
      }
      if ((preg_match('/[^a-z0-9]/', $login ))) {
        form_set_error('ftp_login',t('FTP Login can contain only lowercase english letters and numbers.'));
      }
      if (empty($form_state['values']['ftp_dir'])) {
        form_set_error('ftp_dir',t('FTP Home Direcory cannot be empty.'));
      }
    }
  }

  /**
   * userForm submit callback.
   *
   * @param $form
   * @param $form_state
   * @return bool True if New FTP connection instructions should be generated.
   *
   * This method creates/edits/deletes user FTP account based on user form values.
   * Prints errors and messages with drupal_set_message().
   */
  public function userFormSubmit($form, &$form_state) {
    $account = $form['#user'];
    // Flag set to TRUE if new FTP instructions should be generated and sent to user.
    $return_new_instructions = FALSE;

    $ftp_enable = !empty($form_state['values']['ftp_users_manager_given']);
    $ftp_already_enabled = !empty($account->ftp_username);

    // Create new FTP account.
    if ($ftp_enable && !$ftp_already_enabled) {
      // Errors are printed in low-level methods.
      $return_new_instructions = $this->userFormSubmitCreation($form, $form_state);
    }
    // Edit FTP account password.
    elseif ($ftp_enable && $ftp_already_enabled) {
      $return_new_instructions = $this->userFormSubmitEdit($form, $form_state);
    }
    // Delete FTP account.
    elseif (!$ftp_enable && $ftp_already_enabled) {
      $this->userFormSubmitDeletion($form, $form_state);
    }

    return $return_new_instructions;
  }

  /**
   * Return plain message with FTP connection instructions. Works only for user account form.
   */
  public function userFormConnectionInstructions($form, $form_state, $user_language) {
    $instructions = array();

    $plugin_settings = variable_get('ftp_users_manager_instructions', array('protocol' => '', 'host' => '', 'port'));
    if (!empty($plugin_settings['protocol'])) {
      $instructions[] = t('Protocol', array(), array('langcode' => $user_language)) . ': ' . drupal_strtoupper($plugin_settings['protocol']);
    }
    if (!empty($plugin_settings['host'])) {
      $instructions[] = t('Server', array(), array('langcode' => $user_language)) . ': ' . $plugin_settings['host'];
    }
    if (!empty($plugin_settings['port'])) {
      $instructions[] = t('Port', array(), array('langcode' => $user_language)) . ': ' . $plugin_settings['port'];
    }
    if (!empty($form_state['values']['ftp_login'])) {
      $instructions[] = t('Login', array(), array('langcode' => $user_language)) . ': ' . $this->combineUsername($form_state['values']['ftp_login']);
    }
    if (!empty($form_state['values']['ftp_pass'])) {
      $instructions[] = t('Password', array(), array('langcode' => $user_language)) . ': ' . $form_state['values']['ftp_pass'];
    }

    return implode("\n", $instructions);
  }

  protected function userFormSubmitCreation($form, &$form_state) {
    $username = $this->combineUsername($form_state['values']['ftp_login']);
    $response = $this->createNewUser($username, $form_state['values']['ftp_pass'], $form_state['values']['ftp_dir']);
    if ($response->isSuccess()) {
      db_update('users')->fields(array('ftp_username' => $username))->condition('uid', $form_state['values']['uid'])->execute();
      return TRUE;
    } else {
      drupal_set_message($response->getAnswer(), 'error');
      return FALSE;
    }
  }

  protected function userFormSubmitDeletion($form, &$form_state) {
    $username = $this->combineUsername($form_state['values']['ftp_login']);
    $response = $this->deleteUser($username);
    if ($response->isSuccess()) {
      db_update('users')->fields(array('ftp_username' => ''))->condition('uid', $form_state['values']['uid'])->execute();
      return TRUE;
    } else {
      if ($this->getUserDir($username) === FALSE){
        // There is no such user, we can return TRUE - imulating deletion.
        return TRUE;
      }
      // For some reason user exists but we cannnot delete him.
      drupal_set_message(t($response->getAnswer()), 'error');
      return FALSE;
    }
  }

  protected function userFormSubmitEdit($form, &$form_state) {
    // The only thing we can edit - is a password. So check if it is set.
    if (!empty($form_state['values']['ftp_pass'])) {
      $username = $this->combineUsername($form_state['values']['ftp_login']);
      $response = $this->changePassword($username, $form_state['values']['ftp_pass']);
      if (!$response->isSuccess()) {
        drupal_set_message($response->getAnswer(), 'error');
        return FALSE;
      }
      // TRUE means that some changes were made.
      return TRUE;
    }
  }

  /**
   * @return BegetAPIReponse
   * @see FTPUserManagerHandler::createNewUser()
   */
  public function createNewUser($username, $password, $homeDir) {
    // $this->login_$username should be less than 17 characters long.
    $params = array(
      'input_format' => 'json',
      'input_data' => json_encode(array(
        'suffix' => $this->getSuffixfromUsername($username),
        'password' => $password,
        'homedir' => $homeDir
      ))
    );
    return $this->request('ftp/add', $params);
  }

  /**
   * @return BegetAPIReponse
   * @see FTPUserManagerHandler::createNewUser()
   */
  public function deleteUser($username) {
    $params = array(
      'input_format' => 'json',
      'input_data' => json_encode(array(
        'suffix' => $this->getSuffixfromUsername($username),
      ))
    );

    return $this->request('ftp/delete', $params);
  }

  /**
   * @return BegetAPIReponse
   * @see FTPUserManagerHandler::changePassword()
   */
  public function changePassword($username, $password) {
    $params = array(
      'input_format' => 'json',
      'input_data' => json_encode(array(
        'suffix' => $this->getSuffixfromUsername($username),
        'password' => $password,
      ))
    );
    return $this->request('ftp/changePassword', $params);
  }

  /**
   * Grant SFTP access to FTP user.
   * @param $username
   *    Full FTP user login name
   * @param $status
   *    SSH status: 1 - enable, 0 - disable.
   * @return BegetAPIReponse
   */
  public function toggleSsh($username, $status) {
    $params = array(
      'input_format' => 'json',
      'input_data' => json_encode(array(
        'ftplogin' => $username,
        'status' => $status,
      ))
    );
    return $this->request('user/toggleSsh', $params);
  }

  /**
   * @return BegetAPIReponse
   * @see FTPUserManagerHandler::listUsers()
   */
  public function listUsers() {
    return $this->request('ftp/getList');
  }

  /**
   * Get User home directory.
   *
   * @param string $username
   *  FTP user name for whom Directory should be return
   * @return BegetAPIReponse|string|boolean
   *  If request error happend BegetAPIReponse should be returned, if no user found then
   *  FALSE will be returned, if user found than dir string will be returned.
   */
  protected function getUserDir($username) {
    $response = $this->listUsers();

    if (!$response->isSuccess()) {
      return $response;
    }

    foreach ($response->getAnswer() as $userInfo) {
      if ($userInfo->login == $username) {
        return $userInfo->homedir;
      }
    }

    return FALSE;
  }

  protected function getMaxUsernameLengthWithoutSuffix() {
    return self::MAXUSERNAMELENGTH-strlen($this->login);
  }

  /**
   * Return suffix part of the username without login prefix.
   */
  protected function getSuffixfromUsername($username) {
    return substr($username, strlen($this->login)+1);
  }

  protected function generatePassword() {
    // user_password do not gurantee that there will be number in the password
    // but beget need it, so append number.
    return user_password(5).rand(1,9);
  }

  /**
   * Add prefix to suffix to get real username.
   */
  protected function combineUsername($suffix) {
    return $this->login.'_'.$suffix;
  }

  /**
   * @TODO add info about Ssh granted.
   *
   * Alter userForm() by entering already known information about ftp user.
   */
  protected function userInfoForm(&$form, $username) {
    // Hide login and dir fields.
    $form['ftp_login']['#default_value'] = substr($username, strlen($this->login)+1);
    $form['ftp_login']['#disabled'] = TRUE;
    $form['ftp_login']['#type'] = 'hidden';

    $userDir = $this->getUserDir($username);
    $form['ftp_dir']['#default_value'] = $userDir;
    $form['ftp_dir']['#disabled'] = TRUE;
    $form['ftp_dir']['#type'] = 'hidden';

    // Output user ftp info.
    $form['ftp_info'] = array(
      '#type' => 'container',
      '#weight' => '0',
      'login' => array(
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => '<strong>'.t('FTP Login').'</strong>: '.$username
      ),
      'dir' => array(
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => '<strong>'.t('FTP Home Directory').'</strong>: '.$userDir,
      ),
    );

    return $form;
  }

  /**
   * @return BegetAPIReponse
   */
  protected function request($method, $params = array()) {
    $url = $this->apiUrl.'/' . $method . '?login='.$this->login.'&passwd=' . trim(ftp_users_manager_decrypt($this->password)) . '&output_format=json&';
    $url .= http_build_query($params);

    $result = drupal_http_request($url);

    return new BegetAPIReponse($result->data, 'json');
  }

}

/**
 * @author Niremizov <niremizov@yandex.ru>
 */
class BegetAPIReponse  implements FTPUsersManagerHandlerReponse {
  protected $status;
  protected $rawReponse;

/**
 * Constructor for BegetAPIReponse Class.
 *
 * @param string $response
 *  Reponse from api.beget as described here: http://beget.ru/api_rules
 *
 * @param string $format
 *  (optional) For now only json is supported.
 *
 * @throws Exception
 */
  public function __construct($response, $format = 'json') {
    if (!is_string($response)) {
      throw new Exception('Argument $response passed to BegetAPIReponse must be a string, '.gettype($response).' given.');
    }

    if ($format == 'json') {
      $response = json_decode($response);
    } else {
      throw new Exception('BegetAPIReponse support only JSON format for now, '.$format.' requested.');
    }

    if (!isset($response->status)) {
      throw new Exception('Cannot parse Beget Reponse.');
    }

    $this->status = $response->status;
    $this->rawReponse = $response;
  }

  /**
   * Checks whatever reponse is succesful.
   *
   * @return boolean
   *
   * @see FTPUsersManagerHandlerReponse::isSuccess()
   */
  public function isSuccess() {
    return ($this->status == 'success' && $this->rawReponse->answer->status == 'success') ? TRUE : FALSE;
  }

  /**
   * Get reponse message.
   *
   * @return string
   *
   * @see FTPUsersManagerHandlerReponse::getAnswer()
   */
  public function getAnswer() {
    if ($this->isSuccess()) {
      return $this->rawReponse->answer->result;
    } else if (isset($this->rawReponse->error_text)) {
      return $this->rawReponse->error_text;
    } else {
      return $this->rawReponse->answer->errors[0]->error_text;
    }
  }

}
