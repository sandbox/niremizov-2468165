<?php
/**
 * @file
 * Settings form.
 */

/**
 * Settings form for ftp users manager.
 */
function ftp_users_manager_settings_form($form, &$form_state) {
  ctools_include('plugins');

  $options = array();
  foreach (ctools_get_plugins('ftp_users_manager', 'plugin') as $id => $plugin) {
    $options[$id] = $plugin['label'];
  }

  $form['plugin'] = array(
    '#type' => 'select',
    '#title' => t('Preferred ftp plugin'),
    '#options' => $options,
    '#description' => t('Choosen handler for ftp users manager.'),
    '#default_value' => variable_get('ftp_users_manager', 'beget'),
    '#ajax' => array(
      'wrapper' => 'ftp-users-manager-settings',
      'callback' => 'ftp_users_manager_settings_ajax',
    ),
  );

  $plugin_id = !empty($form_state['values']['plugin']) ? $form_state['values']['plugin'] : variable_get('ftp_users_manager', 'beget');
  $plugin  = $form_state['plugin_instance'] = ftp_users_manager_plugin_instance($plugin_id);
  $form_state['ftp_users_manager_plugin'] = $plugin;

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugin settings'),
    '#prefix' => '<div id="ftp-users-manager-settings">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  // Give control to plugin, so it make it's own alterations.
  $plugin->settingsForm($form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

function ftp_users_manager_settings_ajax($form, &$form_state) {
  return $form['settings'];
}

/**
 * Validate handler for ftp users manager settings form.
 */
function ftp_users_manager_settings_form_validate($form, &$form_state) {
  // Nothing to do here yet.
}

/**
 * Submit handler for ftp users manager settings form.
 */
function ftp_users_manager_settings_form_submit($form, &$form_state) {
  variable_set('ftp_users_manager', $form_state['values']['plugin']);

  $form_state['ftp_users_manager_plugin']->settingsFormSubmit($form, $form_state);

  drupal_set_message(t('FTP Users manager settings saved.'));
}
