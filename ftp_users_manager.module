<?php

//@TODO When user is blocked we shuold delete FTP access also.

/**
 * Implements hook_menu().
 */
function ftp_users_manager_menu() {
  $items = array();

  $items['admin/config/media/ftp_users_manager'] = array(
    'title' => 'FTP Users manager',
    'description' => 'Configure FTP Users manager settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ftp_users_manager_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'ftp_users_manager.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function ftp_users_manager_permission() {
  return array(
    'ftp_users_manager_admin' => array(
      'title' => t('Admin ftp users'),
      'description' => t('Admin ftp users'),
    ),
  );
}

/**
 * Implements hook_schema_alter().
 */
function ftp_users_manager_schema_alter(&$schema) {
  $schema['users']['fields']['ftp_username'] = array(
    'type' => 'varchar',
    'length' => 17,
    'not null' => TRUE,
    'default' => '',
    'description' => 'Unique ftp username.'
  );

  $schema['users']['unique keys']['ftp_username'] = array('ftp_username');
}

/**
 * Inform CTools about the FTP users manager plugin.
 *
 * Implements hook_ctools_plugin_type().
 */
function ftp_users_manager_ctools_plugin_type() {
  $plugins['plugin'] = array(
    'cache' => TRUE,
    'use hooks' => TRUE,
  );

  return $plugins;
}

/**
 * Implements hook_ftp_users_manager_plugin().
 */
function ftp_users_manager_ftp_users_manager_plugin() {
  return array('BegetFTPUsersManager' => array(
    'name' => 'beget',
    'label' => t('Beget'),
    'handler' => array(
      'class' => 'BegetFTPUsersManager',
      'file' => 'BegetFTPUsersManager.inc',
      'path' => drupal_get_path('module', 'ftp_users_manager') . '/includes/plugins'
    ),
  ));
}

/**
 * Get active ftp users manager plugin instance.
 *
 * @return current active plugin instance.
 */
function ftp_users_manager_plugin_instance($plugin_id = NULL) {
  ctools_include('plugins');

  $plugins = ctools_get_plugins('ftp_users_manager', 'plugin');

  if (empty($plugin_id)) {
    $preferred_plugin = variable_get('ftp_users_manager', 'beget');
  } else {
    $preferred_plugin = $plugin_id;
  }

  $plugin = !empty($plugins[$preferred_plugin]) ? $plugins[$preferred_plugin] : reset($plugins);

  return new $plugin['handler']['class'];
}

/**
 * Implements hook_views_data_alter().
 */
function ftp_users_manager_views_data_alter(&$data) {
  $data['users']['ftp_username'] = array(
    'title' => t('FTP Login'),
    'help' => t('The FTP login for current user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
}

/**
 * Add to user_profile_form() fields for settings FTP account.
 *
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function ftp_users_manager_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  ftp_users_manager_userform ($form, $form_state, $form_id);
}

/**
 * Add to user_register_form() fields for settings FTP account.
 *
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function ftp_users_manager_form_user_register_form_alter(&$form, &$form_state, $form_id) {
  ftp_users_manager_userform ($form, $form_state, $form_id);
}

/**
 * Adds FTP user settings inside user forms.
 */
function ftp_users_manager_userform (&$form, &$form_state, $form_id) {
  $plugin = ftp_users_manager_plugin_instance();

  if (!$plugin->setUp()) {
    drupal_set_message('FTP Users manager cannot setup.', 'warning');
    return;
  }

  $form_state['ftp_users_manager_plugin'] = $plugin;
  $user = $form['#user'];

  $form['ftp_users_manager'] = array(
    '#type' => 'fieldset',
    '#title' => t('s/FTP access'),
    '#weight' => 2,
    '#access' => user_access('ftp_users_manager_admin'),
  );
  $form['ftp_users_manager']['ftp_users_manager_given'] = array(
    '#type' => 'checkbox',
    '#title' => t('Give FTP access'),
    '#default_value' => !empty($user->ftp_username),
    '#description' => t('Grants user ability to connect to server\'s folder through FTP protocol.'),
  );
  $form['ftp_users_manager']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(':input[name="ftp_users_manager_given"]' => array('checked' => FALSE))
    ),
  );
  $plugin->userForm($form, $form_state);

  $form['#submit'][] = 'ftp_users_manager_form_user_submit';
  $form['#validate'][] = 'ftp_users_manager_form_user_validate';
}

/**
 * Validation callback for ftp_users_manager_form_user_profile_form_alter().
 * Calls for plugins validation methods.
 *
 * @see user_profile_form()
 */
function ftp_users_manager_form_user_validate($form, &$form_state) {
  $form_state['ftp_users_manager_plugin']->userFormValidate($form, $form_state);
}

/**
 * Submit handler for ftp_users_manager_form_user_profile_form_alter().
 *
 * @see user_profile_form()
 */
function ftp_users_manager_form_user_submit($form, &$form_state) {
  // Reload user object to access fresh data.
  $account = user_load($form_state['values']['uid'], TRUE);

  $send_new_instructions = $form_state['ftp_users_manager_plugin']->userFormSubmit($form, $form_state);

  // Send email with new FTP instructions.
  // @TODO keep an eye on this, probably could be moved to FTPUserManagerHandler::userFormSubmit().
  if (!empty($send_new_instructions)) {
    // Generate instructions by plugin.
    $params['tokens']['@instructions'] = $form_state['ftp_users_manager_plugin']->userFormConnectionInstructions($form, $form_state, $account->language);
    $params['user'] = $account;
    // Send email in user language.
    $message = drupal_mail('ftp_users_manager', 'user_instructions', $account->mail, $account->language, $params);
    if ($message['result']) {
      drupal_set_message(t('FTP instructions were successfully send to @email.', array('@email' => $account->mail)));
    }
  }
}

/**
 * Implements hook_mail().
 */
function ftp_users_manager_mail($key, &$message, $params) {
  if ($key == 'user_instructions') {
    $message['subject'] = t('s/FTP connection instructions');
    $body = t("[user:name],

Use the following instructions to connect to s/FTP server:

@instructions

--  [site:name] team
", $params['tokens'], array('langcode' => $message['language']));

    // Load language object to call token_replace() properly.
    $languages = language_list();
    $language = $languages[$message['language']];
    $body = token_replace($body, array('user' => $params['user']), array('language' => $language));

    $message['body'][] = $body;
  }
}

/**
 * Implements hook_user_delete().
 *
 * @TODO Can be merged with BegetFTPUsersManager::userFormSubmit() - deletion mode.
 */
function ftp_users_manager_user_delete($account) {
  // User was already deleted, delete his ftp user if was set.
  if (empty($account->ftp_username)) {
    return;
  }
  $plugin = ftp_users_manager_plugin_instance();

  if (!$plugin->setUp()) {
    drupal_set_message('FTP Users manager cannot setup.', 'warning');
    drupal_set_message(t('FTP Account for user @user could not be deleted.', array('@user' => $account->name)));
    return;
  }

  $response = $plugin->deleteUser($account->ftp_username);
  if ($response->isSuccess()) {
    drupal_set_message(t('FTP Account for user @user was deleted.', array('@user' => $account->name)));
  } else {
    drupal_set_message(t('FTP Account for user @user could not be deleted.', array('@user' => $account->name)));
  }
}

/**
 * Helper function for encryting data.
 *
 * @see ftp_users_manager_decrypt()
 */
function ftp_users_manager_encrypt($string) {
  global $drupal_hash_salt;
  return mcrypt_encrypt(MCRYPT_RIJNDAEL_256, substr($drupal_hash_salt, 0, 31), $string, MCRYPT_MODE_ECB);
}

/**
 * Helper function for decrypting data, crypted by ftp_users_manager_encrypt().
 *
 * @see ftp_users_manager_encrypt()
 */
function ftp_users_manager_decrypt($string) {
  global $drupal_hash_salt;
  return mcrypt_decrypt(MCRYPT_RIJNDAEL_256, substr($drupal_hash_salt, 0, 31), $string, MCRYPT_MODE_ECB);
}

